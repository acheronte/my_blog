from django.conf.urls import patterns, include, url
from django.contrib import admin
# from django.conf import settings
from settings.blog import local


urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^summernote/', include('django_summernote.urls')),
                       url(r'^markdown', include('django_markdown.urls')),
                       url(r'', include('sites.blog.urls')),)

# if in local environment
if local.DEBUG:
    urlpatterns += patterns('django.views.static', (
        r'^media/(?P<path>.*)', 'serve',
        {'document_root': local.PROJECT_ROOT},
    ))
