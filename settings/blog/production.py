from ..base import *


LOCAL_APPS = (
    'sites.blog',
    'django_summernote',
    'django_markdown',
    'django.contrib.humanize',
    'storages',
    'endless_pagination',
)

INSTALLED_APPS += LOCAL_APPS

# overrides MEDIA_ROOT
DEFAULT_FILE_STORAGE = 'utils.amazon_storage.MediaS3BotoStorage'
# overrides STATIC_ROOT
STATICFILES_STORAGE = 'utils.amazon_storage.StaticS3BotoStorage'

AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", "Defaulting")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", "Not set")
AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_S3_BUCKET_NAME", "So sorry")

S3_URL = 'https://{}.s3.amazonaws.com'.format(AWS_STORAGE_BUCKET_NAME)

STATIC_URL = S3_URL + '/static/'

MEDIA_URL = S3_URL + '/media/'
