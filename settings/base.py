import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS


SETTINGS_DIR = os.path.dirname(__file__)  # base.py location
PROJECT_ROOT = os.path.abspath(os.path.join(SETTINGS_DIR, os.pardir))

DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [os.environ.get('AO_HOST'), os.environ.get('LIM_DB_HOST')]

SECRET_KEY = os.environ.get('SECRET_KEY', 'Defaulting')

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

# Middleware

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'launch_points.ao_wsgi.application'

# Database

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('LIM_DB_ENGINE', 'Defaulting'),
        'NAME': os.environ.get('LIM_DB_NAME', 'Defaulting'),
        'USER': os.environ.get('LIM_DB_USER', 'Defaulting'),
        'PASSWORD': os.environ.get('LIM_DB_PASSWORD'),
        'HOST': os.environ.get('LIM_DB_HOST', 'Deffing'),
        'PORT': os.environ.get('LIM_DB_PORT'),
    }
}

# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static assets

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

MEDIA_URL = '/media/'
# django-summernote stops uploading images, when MEDIA_ROOT is active,
# as it ships with its default upload logic. A probable fix for this,
# would be to make a custom upload_to to match the MEDIA_ROOT's media folder
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)
# needed for endless-pagination
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.template.context_processors.request',
)


