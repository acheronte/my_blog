# The rationale

Repository to document the process of making a Javascript-light website (in this case a personal blog) in Django. This was done to experiment with Amazon's EC2 instance, website development, deployment and server maintenance (The site was live for 1 year which gave me exposure to many interesting aspects like Database backups, security updates, traffic tracking etc.). 

## The Django side of things

Of all the ways to configure Django, the one I prefer is a hybrid approach of [Rob Golding's method](https://code.djangoproject.com/wiki/SplitSettings#RobGoldingsmethod) and [Simple Package Organization for Environments ](https://code.djangoproject.com/wiki/SplitSettings#SimplePackageOrganizationforEnvironments) [split settings guidelines](https://code.djangoproject.com/wiki/SplitSettings). In the settings folder, in ```blog/production.py``` you can see how to not only inherit from a ```base.py``` settings file (say one to be inherited by different environments like a ```dev.py``` settings file on a local/development machine) but add production specific configuration like Amazon's Web Services (in this case S3 and the EC2 that hosts the app.). The code:

``` 
# production.py
 
from ..base import *


LOCAL_APPS = (
    'sites.blog',
    'django_summernote',
    'django_markdown',
    'django.contrib.humanize',
    'storages',
    'endless_pagination',
)

INSTALLED_APPS += LOCAL_APPS

# overrides MEDIA_ROOT
DEFAULT_FILE_STORAGE = 'utils.amazon_storage.MediaS3BotoStorage'
# overrides STATIC_ROOT
STATICFILES_STORAGE = 'utils.amazon_storage.StaticS3BotoStorage'

AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", "Defaulting")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", "Not set")
AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_S3_BUCKET_NAME", "So sorry")

S3_URL = 'https://{}.s3.amazonaws.com'.format(AWS_STORAGE_BUCKET_NAME)

STATIC_URL = S3_URL + '/static/'

MEDIA_URL = S3_URL + '/media/'
```

Also notice in the ```sites/blog``` directory, which demonstrates the philosophy of having different (and separate sites/applications) live together in the same project. 
If for example you want to build another website, you can just create a new folder ```sites/anotherSite``` and re-use the pre-exisiting settings while adding application-specific config. (models, busines logic etc.) at the same time (Same thing for the ```templates``` folder).

On the templates end, there's really not much going on, we have the staples: ```Bootstrap``` and ```jQuery``` plus the ```endless_pagination``` module to have that nice infinite scrolling effect on the main page.

## Server configuration

The important folders are:

* ```launch_points```
* ```sysadmin```

```launch_points``` is the place where
[uwsgi.ini](http://uwsgi-docs.readthedocs.io/en/latest/Configuration.html?highlight=ini#ini-files) uses to launch your Django application code from (```manage.py ``` is not needed with this approach), and specifically ```prod_wsgi.py```. Here's what it looks like:

``` 
# production uwsgi.ini file
[uwsgi]

# Root directory (full path)
# <ubuntu> needs to be replaced with the name of your home folder or wherever you plan to store the sites in the filesystem
chdir           = /home/ubuntu/sites/my_blog
# App module (relative path)
# NB: we're using dot notation to navigate path instead of '/'
module          = launch_points.prod_wsgi
# the virtualenv (full path)
home            = /home/ubuntu/.pyenv/versions/my_blog_env

# master
master          = true
# maximum number of worker processes (this is depends on use case and there's not a definite number)
processes       = 10
# the socket (use the full path to be safe)
socket          = /home/ubuntu/sites/my_blog/sys_admin/my_blog.sock
# ... with appropriate permissions
# 666 very permissive
# 664 more sensible than 666
chmod-socket    = 666
# logs
logto           = /home/ubuntu/logs/my_blog_uwsgi.log
# clear environment on exit (true if the app server doesn't need to be restarted frequently = stable)
vacuum          = true
```

Lastly, in the ```sysadmin``` folder you find the ```nginx.conf``` file:

``` 
  # anthonyokonedo.com site

  # upstream component nginx needs to connect to
  upstream django {
      server unix:///home/ubuntu/sites/my_blog/sys_admin/my_blog.sock;
  }

  # configuration of the server
  server {
      # the port the site will be served on
      listen      80;
      # the domain name it will serve for
      server_name anthonyokonedo.com; # substitute your machine's IP address or FQDN
      charset     utf-8;

      # logging
      access_log /home/ubuntu/logs/access.log;
      error_log  /home/ubuntu/logs/error.log;

      # max upload size
      client_max_body_size 75M;   # adjust to taste

    # all traffic to <domain-name>/blog will be sent directly to memcached server ...
    #location /blog {
    #      memcached_pass unix:///home/ubuntu/sites/my_blog/sys_admin/memcached.sock;
    #      default_type   text/html;
    #      # error_page     404 502 = /;  # ... if content is not in cache send requests to upstream server(s)
    #      set $memcached_key $uri;
    #}

    # serve directly media ...
    location /media/  {
        alias /home/ubuntu/sites/my_blog/media/;
    }
    # ... and static
    location /static/ { 
        alias /home/ubuntu/sites/my_blog/static/;
    }

    # Finally, send all non-media requests to the app server.
    location / {
        uwsgi_pass django;
        include    /home/ubuntu/sites/my_blog/sys_admin/uwsgi_params; 
    }
    
    # what to serve if upstream is not available or crashes
    error_page 400 /static/400.html;
    error_page 403 /static/403.html;
    error_page 404 /static/404.html;
    error_page 500 502 503 504 /static/500.html; 

    # compression
    gzip on;
    gzip_http_version 1.0;
    gzip_comp_level 5;
    gzip_proxied any;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_types text/plain text/html text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;
}
```

