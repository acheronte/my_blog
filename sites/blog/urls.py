from django.conf.urls import url, patterns
from .views import EntryList, EntryDetail

urlpatterns = patterns('sites.blog.views',
                       url(r'^$', EntryList.as_view(), name='index'),
                       url(r'blog/(?P<slug>\S+)$', EntryDetail.as_view(), name='view_post'),)

