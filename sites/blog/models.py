from django.db import models
from django.core.urlresolvers import reverse_lazy


class LiveEntriesManager(models.Manager):  # queryset to fetch only live entries
    def get_queryset(self):
        return super(LiveEntriesManager, self).get_queryset().filter(status=self.model.LIVE)


class Blog(models.Model):
    LIVE = 1
    DRAFT = 2
    HIDDEN = 3
    STATUS_CHOICE = (
        (LIVE, 'Live'),
        (DRAFT, 'Draft'),
        (HIDDEN, 'Hidden')
    )

    title = models.CharField(max_length=100)
    body = models.TextField()

    slug = models.SlugField(unique=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICE)

    # placing default manager first so admin shows all posts, not the live ones only
    objects = models.Manager()
    live_objects = LiveEntriesManager()

    class Meta:
        verbose_name = 'Entry'
        verbose_name_plural = 'Entries'
        ordering = ['-created']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy('view_post', kwargs={'slug': self.slug})



