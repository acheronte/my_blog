from django.views.generic import DetailView
from .models import Blog
from endless_pagination.views import AjaxListView


class EntryList(AjaxListView):
    queryset = Blog.live_objects.all()
    context_object_name = 'live_posts'
    template_name = 'blog/entry_index.html'
    page_template = 'blog/entry_list.html'

    # if page_template doesn't work, pass context explicitly:
    # def get_context_data(self, **kwargs):
    #    context = super(EntryList, self).get_context_data(**kwargs)
    #    context.update({
    #         'page_template': self.page_template,
    #         'all_entries': Blog.objects.all(),
    #          ...
    #    })
    #    return context

class EntryDetail(DetailView):
    model = Blog
    context_object_name = 'post'  # this passes context also
    template_name = 'blog/entry_detail.html'

# pass more context objects to templates:
#    def get_context_data(self, **kwargs):
#        context = super(DetailView, self).get_context_data(**kwargs)
#        context.update({
#            'something': some_queryset_or_similar(),
#        })

